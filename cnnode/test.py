import unittest
from PO.login import Login

from selenium import webdriver

from PO.navbar import Navbar
import time

driver = webdriver.Chrome()

user_name = 'imtest12'
pass_word = '123456'


def login():
    driver.implicitly_wait(6)
    driver.get("http://39.107.96.138:3000/")
    login1 = Login(driver)
    # navbar = Navbar(driver)
    # navbar.go_to_login()
    login1.login_username_password(user_name, pass_word)


class MyTestCase(unittest.TestCase):
    # def test_login(self):
    #     self.assertEqual(True, False)
    def test_check_user_name(self):
        login()
        test_name = driver.find_element_by_css_selector('div.user_card a.dark').text
        # acrual_name='imtest12'
        self.assertEqual(test_name, user_name)

    # def test_str_upper(self):
    #     self.assertEqual('fooo'.upper(), 'FOOO')
    #
    # def test_str_split(self):
    #     self.assertEqual('hello word'.split(), ['hello', 'word'])


if __name__ == '__main__':
    unittest.main()
