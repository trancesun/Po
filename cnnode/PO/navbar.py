from PO.base_page import Base
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver
# from selenium.webdriver.common.action_chains import ActionChains

import time

from selenium import webdriver


class B:
    def __init__(self, driver: WebDriver):
        self.driver = driver


class Navbar(Base):
    _search_link = By.CSS_SELECTOR, '[class="search-query span3"]'
    _index_link = By.CSS_SELECTOR, '[href="/"]'

    _new_user_link = By.CSS_SELECTOR, '[href="/getstart"]'

    _api_link = By.CSS_SELECTOR, '[href="/api"]'
    _about_link = By.LINK_TEXT, '关于'
    _register_link = By.LINK_TEXT, "注册"
    _login_link = By.LINK_TEXT, "登录"

    def go_to_search(self):
        self.driver.find_element(*self._search_link).send_keys('hello')

    def go_to_index(self):
        self.driver.find_element(*self._index_link).click()

    def go_to_new_user(self):
        self.driver.find_element(*self._new_user_link).click()

    def go_to_about(self):
        self.driver.find_element(*self._about_link).click()

    def go_to_register(self):
        self.driver.find_element(*self._register_link).click()

    def go_to_login(self):
        self.driver.find_element(*self._login_link).click()


if __name__ == '__main__':
    driver = webdriver.Chrome()
    driver.implicitly_wait(6)
    navbar=Navbar(driver)

    driver.get("http://39.107.96.138:3000/")
    navbar.login()
    time.sleep(3)

    driver.back()

    navbar.new_user()
