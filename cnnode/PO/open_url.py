from PO.base_page import Base
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

class Open_url(Base):

    def open(self,url="http://39.107.96.138:3000/"):
        self.driver.implicitly_wait(10)
        self.driver.get(url)
