from PO.base_page import Base
from PO.login import Login
from PO.open_url import Open_url
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import random

from selenium.webdriver.common.action_chains import ActionChains


class Issue_topic(Base):
    _issue_topic_button = By.CSS_SELECTOR, '[id="create_topic_btn"]'
    _title = By.CSS_SELECTOR, '[id="title"]'
    _p = '[class="CodeMirror-scroll"]'

    def click_create(self):
        # op = Open_url(self.driver)
        # op.open()
        # self.driver.implicitly_wait(10)
        # lg = Login(self.driver)
        # lg.login_username_password()
        self.driver.find_element(*self._issue_topic_button).click()

    # 代表下拉框的递i个元素
    def choic(self, i):
        ch = self.driver.find_element_by_xpath(f'//select/option[{i}]')
        print(ch.text)
        ch.click()

    # 输入标题

    def send_h(self, str1: str):
        if len(str1) < 10:
            str1 += str(random.random())[2:13 - len(str1)]
        self.driver.find_element(*self._title).send_keys(str1)

    # 输入文章内容
    def send_p(self, str1: str):
        input_p = self.driver.find_element_by_css_selector(self._p)
        ac = ActionChains(self.driver)
        ac.move_to_element(input_p).click().pause(3).send_keys(str1)
        ac.perform()

    # 全选
    def style_font_a(self):
        ac = ActionChains(self.driver)
        # ctrl+a
        ac.key_down(Keys.CONTROL)
        ac.key_down('a')
        ac.key_up(Keys.CONTROL)
        ac.perform()
        return ac

    # 加粗
    def style_font_b(self):
        ac = ActionChains(self.driver)
        # ctrl+b
        ac.key_down(Keys.CONTROL)
        ac.key_down('b')
        ac.key_up(Keys.CONTROL)

        ac.perform()

        return ac

    # 删除框中所有内容

    def delete_all(self):
        ac = self.style_font_a()
        # ac.key_up(Keys.BACKSPACE).perform()
        ac.key_up(Keys.DELETE).perform()

    # 提交

    def submit(self):
        self.driver.find_element_by_css_selector('[type="submit"]').submit()

    # 返回到登录成功界面
    def return_login(self):
        self.driver.find_element_by_css_selector('[title="imtest11"]').click()


if __name__ == "__main__":

    op = Options()
    //家里路径
    op.add_argument(r'user-data-dir=C:\Users\Administrator\AppData\Local\Google\Chrome\User Dat')
    driver = webdriver.Chrome(chrome_options=op)
    driver.get("http://39.107.96.138:3000/")
    it = Issue_topic(driver)
    str1 = "1234567890"
    i = 0
    for j in str1:
        it.click_create()
        it.choic(2 + i % 3)
        it.send_h(f'test{i}')
        it.send_p(f'test{j}:hello')
        it.style_font_a()
        it.style_font_b()
        # it.delete_all()
        it.submit()
        it.return_login()
        i += 1
