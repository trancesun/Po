from selenium.webdriver.common.by import By

from PO.base_page import Base

from PO.navbar import Navbar


class Login(Base):
    _user_name = By.CSS_SELECTOR, '[id="name"]'
    _pass_word = By.CSS_SELECTOR, '[id="pass"]'
    _login_button = By.CSS_SELECTOR, '[value="登录"]'
    _forget_password = By.CSS_SELECTOR, '[id="forgot_password"]'

    def go_to_login(self):
        navbar = Navbar(self.driver)
        navbar.go_to_login()

    def login_username_password(self, username='imtest11', password='123456'):
        self.go_to_login()
        self.driver.find_element(*self._user_name).send_keys(username)
        self.driver.find_element(*self._pass_word).send_keys(password)
        self.driver.find_element(*self._login_button).click()

    def go_to_forget_password(self):
        self.go_to_login()
        self.driver.find_element(*self._forget_password).click()
